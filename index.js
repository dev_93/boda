const fs = require('fs');
const sharp = require('sharp');
const path = require('path')

const allowExt = ['.jpeg', '.jpg', '.png'];
const pathImagen = './assets/images/';


if (fs.existsSync(pathImagen + 'minis')) { 
  fs.rmdirSync(pathImagen + 'minis', {recursive: true})
  fs.mkdirSync(pathImagen + 'minis')
  
} else {
  fs.mkdirSync(pathImagen + 'minis')
}

fs.readdir(pathImagen, {withFileTypes: true}, (err, files) => {
  if (err) {
    console.log('error obteniendo archivos')
    return;
  }
  console.log('Los archivos recuperados son');
  console.log(files);

  for (file of files) {
    
    if (file.isFile()) {
      const extension = path.extname(file.name);
      if (allowExt.includes(extension)) {
        console.log('el path sera', pathImagen + 'minis/' + file.name)
        sharp(pathImagen + file.name)
          .resize(100)
          .toFile(pathImagen + 'minis/' + file.name, (err, info) => {
            if (err) {
              console.log('error en el archivo', err)
              return;
            } else {
              console.log('reducido el archivo', info)
            }
          })
      }
    }

  }

})

